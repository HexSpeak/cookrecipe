var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
//var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
var async =require('async');
var crypto = require('crypto');
var config = require('./config');
var mongoose =require('mongoose');
var facebookAuth = require('./app/facebookAuth');
var googleAuth = require('./app/googleAuth');
var linkedinAuth = require('./app/linkedinAuth');

mongoose.connect(config.database, function(err){
    if(err)
    {
      console.log(err);
    }else{
      console.log('database connected');
    }
});



var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

app.use(express.static(__dirname + '/public'));

app.post('/auth/facebook', facebookAuth);

app.post('/auth/linkedin', linkedinAuth);

app.post('/auth/google', googleAuth);


var api = require('./app/routes/api')(app, express);
app.use('/api', api);

app.get('*', function(req, res){
    res.sendFile(__dirname + '/public/app/views/index.html');
    //res.locals.user = req.user || null;
    //next();
});




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var port = process.env.PORT || 3000;
app.listen(port);


module.exports = app;
