
angular.module("appRoutes",["ui.router", "satellizer"])
.config(function($stateProvider, $urlRouterProvider, $authProvider){

    $urlRouterProvider.otherwise("/");
    $stateProvider
    .state("home",{
        url:"/",
        templateUrl:"app/views/pages/home.html",
        controller:"MainCtrl as main"
    })
    .state("login",{
        url:"/login",
        templateUrl:"app/views/pages/login.html"
    })
    .state("signup",{
        url:"/signup",
        templateUrl:"app/views/pages/signup.html",
        controller:"UserCreateCtrl as user"
    })
    .state("profile",{
        url:"/profile",
        templateUrl:"app/views/pages/profile.html"
    })
    .state("logout",{
        url:"/logout",
        templateUrl:"app/views/pages/logout.html"
    }).state("forgot",{
            url:"/forgot",
            templateUrl:"app/views/pages/forgot.html"
        })
        .state("reset",{
            url:"/reset/:token",
            templateUrl:"app/views/pages/reset.html"
        })
    .state("main",{
        url:"/main",
        templateUrl:"app/views/pages/main.html"
    });
    $authProvider.google({
      clientId: '949155061557-qubledcslj0jq000gos9nmg83iv1rjv5.apps.googleusercontent.com',
      url: 'http://localhost:3000/auth/google'
    });

    $authProvider.facebook({
      clientId: '1748932678674985',
      url: 'http://localhost:3000/auth/facebook'
    });

    $authProvider.linkedin({
      clientId: '75u5o1o9jljdc1',
      url: 'http://localhost:3000/auth/linkedin'
    });
})
.constant('API_URL', "http://localhost:3000/") // not working
.run(function($window){
    var params = $window.location.search.substring(1);
    if(params && $window.opener && $window.opener.location.origin === $window.location.origin){
        var pair = params.split("=");
        var code = decodeURIComponent(pair[1]);
        $window.opener.postMessage(code, $window.location.origin);
    }
});

/*angular.module("appRoutes",["ngRoute"])
        .config(function($routeProvider,$locationProvider){
            $routeProvider
                .when('/',{
                    templateUrl:"app/views/pages/home.html"
                })
                .when('/login',{
                    templateUrl:"app/views/pages/login.html"
                })
            when('/signup',{
                templateUrl:"app/views/pages/signup.html"
            });

            $locationProvider.html5Mode(true);
        });*/