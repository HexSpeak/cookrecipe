

(function (){

    "use strict";

    angular.module("cookRecipe")
        .controller('ProductListCtrl',ProductListCtrl );

    function ProductListCtrl(){
            var vm = this;

        vm.products = [
            {
                "productId": 1,
                "productName":"rump whole",
                "productBrand":"angus",
                "price":15.50,
                "amount":1,
                "unit":"Kg",
                "packaging":"Box"
            },
            {
                "productId": 2,
                "productName":"tomato",
                "productBrand":"",
                "price":10.50,
                "amount":1,
                "unit":"Kg",
                "packaging":"Bag"
            },
            {
                "productId": 3,
                "productName":"arrow root",
                "productBrand":"karo krus",
                "price":15.50,
                "amount":.50,
                "unit":"Kg",
                "packaging":"Packet"
            },
            {
                "productId": 4,
                "productName":"sea salte",
                "productBrand":"saxa",
                "price":5.50,
                "amount":1,
                "unit":"gm",
                "packaging":"Packet"
            },
            {
                "productId": 1,
                "productName":"rump whole",
                "productBrand":"angus",
                "price":15.50,
                "amount":1,
                "unit":"Kg",
                "packaging":"Box"
            },
            {
                "productId": 2,
                "productName":"tomato",
                "productBrand":"",
                "price":10.50,
                "amount":1,
                "unit":"Kg",
                "packaging":"Bag"
            },
            {
                "productId": 3,
                "productName":"arrow root",
                "productBrand":"karo krus",
                "price":15.50,
                "amount":.50,
                "unit":"Kg",
                "packaging":"Packet"
            },
            {
                "productId": 4,
                "productName":"sea salte",
                "productBrand":"saxa",
                "price":5.50,
                "amount":1,
                "unit":"gm",
                "packaging":"Packet"
            }
        ];


        }

}());


