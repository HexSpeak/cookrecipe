angular.module("cookRecipe")
    .controller("MainCtrl", function($rootScope, $location, Auth, $auth){
            var vm= this;
            //vm.isLoggedIn = Auth.isLoggedIn();

        //event listener
        $rootScope.$on('$routeChangeStart', function(){
            //vm.isLoggedIn = Auth.isLoggedIn();
            Auth.getUser()
                .then(function(data){
                    vm.user=data.data;
                });
        });

        vm.doLogin =function(){
            vm.processing =true;
            vm.error='';
            Auth.login(vm.loginData.username, vm.loginData.password)
                .success(function(data){
                    vm.processing = false;
                    Auth.getUser()
                        .then(function(data){
                            vm.user= data.data;
                        });
                    if(data.success)
                        $location.path('/main');
                    else
                        vm.error= data.message;
                });
        }
        vm.doLogout = function(){
            Auth.logout();
            $location.path('/logout');
        }

        vm.isLoggedIn = function(){
            return Auth.isLoggedIn();
            //return $auth.isAuthenticated;
        };

        vm.authenticate = function(provider){
            $auth.authenticate(provider).then(function(res){
                    vm.processing = false;
                    Auth.getUser()
                        .then(function(data){
                            console.log(data);
                            vm.user=data.data;
                        });
                        console.log(res.data);
                    if(res.data.success){
                        $location.path('/main');
                    }
                    else
                        vm.error= res.data.message;
                });
        };

    });