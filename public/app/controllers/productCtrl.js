/**
 * Created by GuruG on 5/5/2016.
 */

(function(){
    "use strict"

    angular.module("cookRecipe")
        .controller("ProductCtrl",function(Product,$stateParams,$window){
            var vm = this;

            vm.processing = true;

            // for all list of product
            Product.all()
                .success(function(data){
                    vm.processing = false;
                    vm.products= data.data;
                });

            // delete single product
            vm.deleteProduct = function(id){
                vm.processing = true;
                Product.delete(id)
                    .success(function(data){
                        Product.all()
                            .success(function(data){
                                vm.processing = false;

                                vm.products = data.data;
                            });
                    });
            };

            // create product

            vm.saveProduct = function(){
                vm.processing = true;
                vm.message =' ';

                Product.createProduct(vm.productData)
                    .then(function(response){
                        vm.processing = false;
                        vm.productData= {};
                        vm.message = response.data.message;
                        $window.localStorage.setItem('token',response.data.token);
                        //$location.path('/confirmEmail');
                    });
            };

            // update

            Product.get($stateParams.product_id)
                .success(function(data){
                    vm.productData = data;
                });

            //function to save user
            vm.saveProduct = function(){
                vm.processing = true;
                vm.message = '';
                // call the userService to update
                Product.update($stateParams.product_id, vm.productData)
                    .success(function(data){
                        vm.processing = false;
                        vm.productData = {};
                        vm.message = data.message;
                    });
            };

        });

}());
