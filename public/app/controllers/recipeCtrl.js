/**
 * Created by GuruG on 5/5/2016.
 */


(function(){
    "use strict"

    angular.module("cookRecipe")
        .controller("RecipeCtrl",function(Recipe,$stateParams,$window){
            var vm = this;

            vm.processing = true;

            // for all list of recipe
            Recipe.all()
                .success(function(data){
                    vm.processing = false;
                    vm.recipies= data.data;
                });

            // delete single recipe
            vm.deleteRecipe = function(id){
                vm.processing = true;
                Recipe.delete(id)
                    .success(function(data){
                        Recipe.all()
                            .success(function(data){
                                vm.processing = false;

                                vm.recipies = data.data;
                            });
                    });
            };

            // create recipe

           vm.saveRecipe = function(){
                vm.processing = true;
                vm.message =' ';

                Recipe.createRecipe(vm.recipeData)
                    .then(function(response){
                        vm.processing = false;
                        vm.recipeData= {};
                        vm.message = response.data.message;
                        $window.localStorage.setItem('token',response.data.token);
                        //$location.path('/confirmEmail');
                    });
            };

            // update

            Recipe.get($stateParams.recipe_id)
                .success(function(data){
                    vm.recipeData = data;
                });

            //function to save user

            vm.saveRecipe = function(){

                vm.processing = true;

                vm.message = '';
                // call the userService to update
                Recipe.update($stateParams.recipe_id, vm.recipeData)
                    .success(function(data){
                        vm.processing = false;
                        vm.recipeData = {};
                        vm.message = data.message;
                    });
            };

        });

}());
