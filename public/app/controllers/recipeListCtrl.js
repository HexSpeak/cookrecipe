
(function (){

    "use strict";

    angular.module("cookRecipe")
        .controller("RecipeListCtrl",RecipeListCtrl);

            function RecipeListCtrl(){
                var vm = this;


                 vm.recipies = [
                    {
                        "recipeId": 1,
                        "recipeName":"Tomato sauce",
                        "recipeTag":"Italian",
                        "yield":1.00,
                        "unit":"it",
                        "price":10.5
                    },

                    {
                        "recipeId": 2,
                        "recipeName":"pasta penni",
                        "recipeTag":"italian",
                        "yield":2,
                        "unit":"it",
                        "price":5.56
                    },
                    {
                        "recipeId": 3,
                        "recipeName":"basil pesto",
                        "recipeTag":"italian",
                        "yield":1,
                        "unit":"Kg",
                        "price":20.6
                    },
                    {
                        "recipeId": 4,
                        "recipeName":"test",
                        "recipeTag":"",
                        "yield":3,
                        "unit":"Kg",
                        "price":5.5
                    }];
            }

}());

