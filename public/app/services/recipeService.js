/**
 * Created by GuruG on 5/5/2016.
 */



(function(){
    "use strict";
    angular.module("recipeService",[])
        .factory("Recipe",function($http){
            var  recipeFactory = {};

            // get complete list of recipes in DB
            recipeFactory.all = function(){
                return $http.get('/api/recipies/');
            };

            // get single recipe from DB

            recipeFactory.get= function(id){
                return $http.get('/api/recipies/' +id);
            };

            //create recipe
            recipeFactory.createRecipe = function(recipeData){
                return $http.post('/api/recipie/', recipeData);
            };

            // update recipe in DB
            recipeFactory.update = function(id, recipeData){
                return $http.put('/api/recipies/'+id, recipeData);
            };

            //delete recipe from DB

            recipeFactory.delete = function(id){
                return $http.remove('/api/recipies/' +id);
            };

            return recipeFactory;
        }
        );

}());
