/**
 * Created by GuruG on 5/5/2016.
 */



(function(){
    "use strict";
    angular.module("productService",[])
        .factory("Product",function($http){
            var  productFactory = {};

            // get complete list of products in DB
            productFactory.all = function(){
                return $http.get('/api/products/');
            };

            // get single product from DB

            productFactory.get= function(id){
                return $http.get('/api/products/' +id);
            };

            // create a product
            productFactory.createProduct = function(productData){
                return $http.post('/api/product/', productData);
            };

            // update product in DB
            productFactory.update = function(id, productData){
                return $http.put('/api/products/'+id, productData);
            };

            //delete product from DB

            productFactory.delete = function(id){
                return $http.remove('/api/products/' +id);
            };

            return productFactory;
        }
        );

}());
