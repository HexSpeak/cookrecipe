//  used to fetch the api data from server


   angular.module('authService',["satellizer"])
       .factory('Auth',function($http, $q, AuthToken, $window, $auth){

            // variable that hold all data returned from api
           var authFactory ={};

           // return the login api & saved the user for front end
           authFactory.login = function(username, password,email){

               return $http.post('/api/login',{
                   username:username,
                   email:email,
                   password:password
               })
                   .success(function(data){ // to get the token
                        AuthToken.setToken(data.token);
                         return data;
                   });
           }

           authFactory.logout = function(){
              $auth.logout();
               AuthToken.setToken();
           }

           // to check that user have the token or not
           authFactory.isLoggedIn = function(){
                if(AuthToken.hasToken()) return true;
                else return false;
           }
           // to get the information of user
           authFactory.getUser = function(){
               if(AuthToken.getToken())
                    return $http.get('/api/me');

               else
                    return $q.reject({message :"User don't have the token"});
           }

           /*authFactory.googleAuth = function(){
              var clientId = '949155061557-qubledcslj0jq000gos9nmg83iv1rjv5.apps.googleusercontent.com';
              var urlBuilder = [];
              urlBuilder.push("response_type=code",
                "client_id=" + clientId,
                "redirect_uri=" + $window.location.origin,
                "scope=profile email");
              var url = "https://accounts.google.com/o/oauth2/v2/auth?" + urlBuilder.join("&");
              var options = "width=500, height=500, left=" + ($window.outerWidth - 500) / 2 + ", top=" + ($window.outerHeight - 500) / 2.5;

              var deferred = $q.defer();

              var popup = $window.open(url, "", options);
              $window.focus();
              $window.addEventListener('message', function(event){
                if(event.origin === $window.location.origin){
                  var code = event.data;
                  popup.close();
                  return $http.post('/auth/google', { 
                    code: code,
                    clientId: clientId,
                    redirectUri: $window.location.origin
                  }).success(function(data){ // to get the token
                        AuthToken.setToken(data.token);
                        deferred.resolve(data);
                   });
                }
              });
              return deferred.promise;
           }*/

           return authFactory;

       })
        // get the token from browser using $window
       .factory('AuthToken', function($window){

           var authTokenFactory = {};
            authTokenFactory.getToken = function(){
                $window.localStorage.getItem('satellizer_token');
            }

            authTokenFactory.hasToken = function(){
                return ($window.localStorage.getItem('satellizer_token') !== null) ? true : false;
            }

            authTokenFactory.setToken= function(token){
                if(token)
                $window.localStorage.setItem('satellizer_token',token);
                else
                $window.localStorage.removeItem('satellizer_token');
            }
           return authTokenFactory;
       })
        // to check whether the token is valid or not
       .factory("AuthInterceptor", function($location,$q, AuthToken ){
           var authInterceptorFactory = {};

           authInterceptorFactory.request = function(config){
               var token = AuthToken.getToken();
               if(token){
                   config.headers['x-access-token']=token;
               }
                 return config;
           };
             // return the user to login page if he doesn't have valid token
           authInterceptorFactory.responseError = function(response){
               if(response.status == 403)
                    $location.path('/login');
                    return $q.reject(response);
           }

           return authInterceptorFactory;
       });
