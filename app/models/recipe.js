/**
 * Created by GuruG on 5/4/2016.
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var RecipeSchema = new Schema({
   // creator:{type: Schema.Types.ObjectId, ref:'User'},
    recipeName:{type:String, required:true},
    recipeTag:{type:String},
    yield:{type:Number},
    unit:{type:String},
    price:{type:String},
    priceExcluding:{type:Number},
    category:{type:String},
    cost:{type:Number},
    profitMargin:{type:Number}

});

 module.exports = mongoose.model('Recipe',RecipeSchema);


