
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var Schema = mongoose.Schema;

var UserSchema = new Schema({

    name: String,
    username :{type: String, required:true, index:{unique:true}},
    password:{type:String, required:true, select:false},
    email: { type: String, required: true, unique: true },
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    googleId: String,
    facebookId: String,
    linkedinId: String
});

var User = module.exports = mongoose.model('User', UserSchema);
/*UserSchema.pre('save',function(next){
        var user= this;
        if(!user.isModified('password')) return next();
        bcrypt.hash(user.password, null, null , function(err, hash){
            if(err) return next(err);
            user.pssword= hash;
            next();
        });

});

 module.exports.comparePassword= function(candidatePassword, hash, callback){
 bcrypt.compare(candidatePassword, hash, function(err, isMatch){
 if(err) return callback(err);
 callback(null, isMatch);
 });
 }

 */


module.exports.comparePassword = function(password,hash,callback){
      //var user = this;
     bcrypt.compare(password, hash,function(err, isMatch){
         if(err) return callback(err);
         callback(null, isMatch);
     });

}



module.exports.createUser = function(newUser, callback){
    //var newUser = this;
    //if(!newUser.isModified('password')) return next();
    bcrypt.hash(newUser.password, null, null, function(err, hash) {
        if (err) throw err;
        newUser.password = hash;

        newUser.save(callback);
    });

}


