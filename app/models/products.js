/**
 * Created by GuruG on 2/23/2016.
 */

var mongoose = require('mongoose');


var Schema = mongoose.Schema;

var ProductSchema = new Schema({
   // creator:{type: Schema.Types.ObjectId, ref:'User'},
    productName :{type:String},
    brandName:{type:String},
    price:{type:Number},
    amount:{type:Number},
    unit:{type:String},
    packaging:{type:String}

});


 module.exports = mongoose.model('Product', ProductSchema);