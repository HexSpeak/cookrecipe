var config = require('../config');
var request = require('request');
var createToken = require('./createjwt');
var User = require('./models/user');

module.exports = function(req, res){
	var accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken';
	var peopleApiUrl = 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url)';

	var params = {
		client_id: req.body.clientId,
		redirect_uri: req.body.redirectUri,
		client_secret: config.linkedinsecret,
		code: req.body.code,
		grant_type: 'authorization_code'
	};

	request.post(accessTokenUrl, { form: params, json: true }, function(err, response, body) {
    if (response.statusCode !== 200) {
      return res.status(response.statusCode).send({ message: body.error_description });
    }
    var params = {
      oauth2_access_token: body.access_token,
      format: 'json'
    };

    // Step 2. Retrieve profile information about the current user.
    request.get({ url: peopleApiUrl, qs: params, json: true }, function(err, response, profile) {
        User.findOne({ linkedin: profile.id }, function(err, foundUser) {
    			if(err) throw err;
    			if(foundUser){
    				var token = createToken(foundUser);
                    res.json({
                        success:true,
                        message: "login successful",
                        token:token
                    });
    			} else {
        			var newUser = new User();
        			newUser.linkedinId = profile.id;
        			newUser.name = profile.name;
        			newUser.username = profile.id;
        			newUser.password = "1234";

        			var token = createToken(newUser);
		            User.createUser(newUser,function(err, user){
		                if(err){
		                    res.send(err);
		                    return;
		                }
		                res.json({
		                    success:true,
		                    message: "user created",
		                    token:token
		                    });
		                console.log(user);
		            });
    			}
			});
		});
	});
};