var config = require('../config');
var request = require('request');
var createToken = require('./createjwt');
var User = require('./models/user');

module.exports = function(req, res){
	var url = 'https://www.googleapis.com/oauth2/v4/token';
	var apUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
	var params = {
		client_id: req.body.clientId,
		redirect_uri: req.body.redirectUri,
		code: req.body.code,
		grant_type: 'authorization_code',
		client_secret: config.googlesecret
	};
    //console.log(req.body.code);
    request.post(url, {
    	json: true,
    	form: params
    }, function(err, response, token){
    	if(err) throw err;
    	var accessToken = token.access_token;
    	var headers = {
    		Authorization: 'Bearer ' + accessToken
    	};

    	request.get({
    		url: apUrl,
    		headers: headers,
    		json: true
    	}, function(err, response, profile){
    		//console.log(profile);
    		if(err) throw err;
    		User.findOne({ googleId: profile.sub }, function(err, foundUser){
    			if(err) throw err;
    			if(foundUser){
    				var token = createToken(foundUser);
                    res.json({
                        success:true,
                        message: "login successful",
                        token:token
                    });
    			} else {
        			var newUser = new User();
        			newUser.googleId = profile.sub;
        			newUser.name = profile.name;
        			newUser.username = profile.sub;
        			newUser.password = "1234";

        			var token= createToken(newUser);
		            User.createUser(newUser,function(err, user){
		                if(err){
		                    res.send(err);
		                    return;
		                }
		                res.json({
		                    success:true,
		                    message: "user created",
		                    token:token
		                    });
		                console.log(user);
		            });
    			}
    		});
    	});
    });
};