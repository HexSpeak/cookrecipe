var jsonwebtoken = require('jsonwebtoken');
var config = require('../config');

var secretKey = config.secretKey;

module.exports = function (user){

    var token = jsonwebtoken.sign({
        id: user.id,
        name: user.name,
        username: user.username
    }, secretKey,{
        expiresInMinutes:1440
    });
    return token;
};