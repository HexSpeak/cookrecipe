
var User = require('../models/user');
var Recipe = require('../models/recipe');
var Product = require('../models/products');
var Story = require('../models/story');
var config = require('../../config');

var secretKey = config.secretKey;

var jsonwebtoken = require('jsonwebtoken');

var createToken = require('../createjwt');


module.exports = function(app, express){

        var api = express.Router();

        api.post('/signup', function(req, res){
             var newUser = new User  ({
                 name: req.body.name,
                 username: req.body.username,
                 password: req.body.password,
                 email:req.body.email
             });

          /* User.save(function(err){
                if(err){
                    res.send(err);
                    return;
                }
                res.json({message: "user created"});
            });*/
              var token= createToken(newUser);
            User.createUser(newUser,function(err, user){
                if(err){
                    res.send(err);
                    return;
                }
                res.json({
                    success:true,
                    message: "user created",
                    token:token
                    });
                console.log(user);
            });

        });
    api.get('/users', function(req, res){
        User.find({},function(err, users){

            if(err){
                res.send(err);
                return;
            }
            res.json(users);
        });
    });

    api.post('/login', function(req, res){
        User.findOne({
            username:req.body.username

        }).select('name username password').exec(function(err,user){
            if(err) throw err;
            if(!user){
                res.send({message: "user doesn't exist"});
            }
            /*else if(user){
               var validPassword =  user.comparePassword(req.body.password);
                    if (!validPassword)
                    {
                        res.send({message:"invalid password"});
                    }
                    else {
                        var token = createToken(user);

                        res.json({
                            success:true,
                            message: "login successful",
                            token:token
                        });
                    }
                }*/
            else if(user){
                User.comparePassword(req.body.password, user.password, function(err, isMatch){
                    if(err) throw err;
                    if(isMatch){
                        var token = createToken(user);

                        res.json({
                            success:true,
                            message: "login successful",
                            token:token
                        });
                    }else {
                        console.log('Invalid Password');
                        return done(null, false,{message:"Invalid password"});
                    }
                });
            }

        });
    });

   api.use(function(req, res,next){

        console.log("somebody just logged in");
        var token= req.body.token || req.param('token') || req.headers['x-access-token'];
        //check if token exist

        if(token){
            jsonwebtoken.verify(token, secretKey, function(err, decoded){
                if(err){
                    res.status(403).send({success: false, message:"failed to authenticate user"});
                }else{
                    req.decoded= decoded;
                    next();
                }
            });
        }else{
            res.status(403).send({success:false, message:"no token provided"});
        }
    });

    api.route('/recipe')
        .post(function(req, res) {
        var newRecipe = new Recipe({
            creator: req.decoded.id,
            recipeName: req.body.recipeName,
            recipeTag: req.body.recipeTag,
            yield: req.body.yield,
            unit: req.body.unit,
            price: req.body.price,
            priceExcluding: req.body.priceExcluding,
            category: req.body.category,
            cost: req.body.cost,
            profitMargin: req.body.profitMargin
        });
        newRecipe.save(function(err){
            if(err){
                res.send(err);
                return;
            }
            res.json({message: "new recipe created"});
        });

    });
        api.get('/recipies',function(req, res){
            Recipe.find({}, function(err, recipies){
                if(err) res.send(err);
                res.json(recipies);
            });
        });

    api.route('/recipies/:recipe_id')
        .get(function(req, res){
            Recipe.findById(req.params.recipe_id, function(err, recipe){
                if(err) res.send(err);
                res.json(recipe);
            });
        })
        .put(function(req, res){
            Recipe.findById(req.params.recipe_id, function(err, recipe){
                if(err) res.send(err);

                if(req.body.recipeName) recipe.recipeName = req.body.recipeName;
                if(req.body.recipeTag) recipe.recipeTag = req.body.recipeTag;
                if(req.body.yield) recipe.yield = req.body.yield;
                if(req.body.unit) recipe.unit = req.body.unit;
                if(req.body.price) recipe.price = req.body.price;
                if(req.body.priceExcluding) recipe.priceExcluding = req.body.priceExcluding;
                if(req.body.category) recipe.category = req.body.category;
                if(req.body.cost) recipe.cost = req.body.cost;
                if(eq.body.profitMargin) recipe.profitMargin = eq.body.profitMargin;

                recipe.save(function(err){
                    if(err) res.send(err);
                    return res.json({
                        success:true,
                        message:"Recipe updated"
                    });
                });

            });
        })
        .delete(function(req, res){
            Recipe.remove({_id:req.params.recipe_id}, function(err, recipe){
                if(err) res.send(err);
                res.json({
                    success:true,
                    message:"Recipe has been removed"
                });
            });
        });

    api.route('/product')
        .post(function(req, res) {
            var newProduct   = new Product ({
                creator: req.decoded.id,
                productName : req.body.productName,
                brandName : req.body.brandName,
                price : req.body.price,
                amount :req.body.amount,
                unit : req.body.unit,
                packaging : req.body.packaging

            });
            newProduct.save(function(err){
                if(err){
                    res.send(err);
                    return;
                }
                res.json({message: "new product created"});
            });

        });
    api.get('/products',function(req, res){
        Product.find({}, function(err, products){
            if(err) res.send(err);
            res.json(products);
        });
    });

    api.route('/products/:product_id')
        .get(function(req, res){
            Product.findById(req.params.product_id, function(err, product){
                if(err) res.send(err);
                res.json(product);
            });
        })
        .put(function(req, res){
            Product.findById(req.params.product_id, function(err, product){
                if(err) res.send(err);

                if(req.body.productName) product.productName = req.body.productName;
                if(req.body.brandName) product.brandName = req.body.brandName;
                if(req.body.price) product.price = req.body.price;
                if(req.body.amount) product.amount = req.body.amount;
                if(req.body.unit) product.unit = req.body.unit;
                if(req.body.packaging) product.packaging = req.body.packaging;

                product.save(function(err){
                    if(err) res.send(err);
                    return res.json({
                        success:true,
                        message:"Product updated"
                    });
                });

            });
        })
        .delete(function(req, res){
            Product.remove({_id:req.params.product_id}, function(err, product){
                if(err) res.send(err);
                res.json({
                    success:true,
                    message:"product has been removed"
                });
            });
        });

    api.route('/')
        .post(function(req, res){
            var story = new Story({
                creator:req.decoded.id,
                content:req.body.content
            });

            story.save(function(err){
                if(err){
                    res.send(err);
                    return;
                }
                res.json({message:"new story created"});

            });
        })
        .get(function(req,res){
            Story.find({creator:req.decoded.id}, function(err, stories){
                if(err){
                    res.send(err);
                    return;
                }
                res.json(stories);
            });
        });

    api.get('/me', function(req, res){
        res.json(req.decoded);
    });

    return api;
}
