/**
 * Created by GuruG on 5/4/2016.
 */

var Product = require('../models/product');
var Recipe = require('../models/recipe');


module.exports = function(app, express){

    var productRecipe = express.Router();

    productRecipe.post('/recipe', function(req, res){
                    var newRecipe = new Recipe ({
                        creator:req.decoded.id,
                        recipeName :req.body.recipeName,
                        recipeTag : req.body.recipeTag,
                        yield: req.body.yield,
                        unit :req.body.unit,
                        price:req.body.price,
                        priceExcluding : req.body.priceExcluding,
                        category : req.body.category,
                        cost : req.body.cost,
                        profitMargin: req.body.profitMargin
                    });

        newRecipe.save();
    });

}
