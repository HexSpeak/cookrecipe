var config = require('../config');
var request = require('request');
var qs = require('querystring');
var createToken = require('./createjwt');
var User = require('./models/user');

module.exports = function(req, res){
	var accessTokenUrl = 'https://graph.facebook.com/oauth/access_token';
	var graphApiUrl = 'https://graph.facebook.com/me';

	var params = {
		client_id: req.body.clientId,
		redirect_uri: req.body.redirectUri,
		client_secret: config.facebooksecret,
		code: req.body.code
	};

	request.get({ 
		url: accessTokenUrl, 
		qs:params 
	}, function(err, response, accessToken){
		accessToken = qs.parse(accessToken);

		request.get({url: graphApiUrl, qs: accessToken, json: true}, function(err, response, profile){
			User.findOne({facebookId: profile.id}, function(err, foundUser){
    			if(err) throw err;
    			if(foundUser){
    				var token = createToken(foundUser);
                    res.json({
                        success:true,
                        message: "login successful",
                        token:token
                    });
    			} else {
        			var newUser = new User();
        			newUser.facebookId = profile.id;
        			newUser.name = profile.name;
        			newUser.username = profile.id;
        			newUser.password = "1234";

        			var token = createToken(newUser);
		            User.createUser(newUser,function(err, user){
		                if(err){
		                    res.send(err);
		                    return;
		                }
		                res.json({
		                    success:true,
		                    message: "user created",
		                    token:token
		                    });
		                console.log(user);
		            });
    			}
			});
		});
	});
};